package foo

import (
    "github.com/google/uuid"
)

func Foo() string {
    return "foo, bar, baz are common placeholder-names"
}

func UuidFoo() (string, string) {
    u := uuid.New()
    us := u.String()
    
    return us, us + "_" + Foo()
}
