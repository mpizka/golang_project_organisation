package foo

import "testing"

func TestFoo(t *testing.T) {
    expect := "foo, bar, baz are common placeholder-names"

    if got := Foo(); got != expect {
        t.Errorf("expect: %q got: %q", expect, got)
    }
}

func TestUuidFoo(t *testing.T) {
    expect := "foo, bar, baz are common placeholder-names"

    u, uf := UuidFoo()

    if uf != u + "_" + expect {
        t.Errorf("expect: %q got: %q", u + "_" + expect, uf)
    }

}
