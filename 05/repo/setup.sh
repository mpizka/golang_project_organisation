#! /bin/bash
IMPORT_PATH="127.0.0.1/foo.git"

if [[ -n "$1" ]]; then
    IMPORT_PATH="$1"
fi

# init repository
git init .

# init go project
go mod init "$IMPORT_PATH"

# init foo.go
cat > foo.go <<EOF
// Package foo provides foo'ish functionality
package foo


import (
    "math/rand"
)

var quotes = []string {
    "99 little bugs in the code. 99 little bugs in the code. Take one down, patch it around. 127 little bugs in the code...",
    "Always code as if the person who ends up maintaining your code will be a violent psychopath who knows where you live.",
    "How many programmers does it take to change a light bulb? None, that’s a hardware problem.",
    "When I wrote this code, only God and I understood what I did. Now only God knows.",
    "One man’s crappy software is another man’s full-time job.",
    "Documentation is like sex: When it is bad, it is better than nothing.",
    "What’s the object-oriented way to get wealthy? Inheritance.",
    "There are 2 hard things in Programming: naming things, cache invalidation, and off-by-one errors",
    "There are 10 types of people in the world: Those who understand binary, and those who don't.",
    "There’s no place like 127.0.0.1.",
}

func Quote() string {
    return quotes[rand.Intn(len(quotes))]
}
EOF

# init .gitignore
cat > .gitignore <<EOF
*.sh
*.swp
*.patch
*.idea
*.note
a.out
EOF

# v1.0.0 : commit and tag 
git add .gitignore *
git commit -m "add v1.0.0"
git tag v1.0.0

# v1.1.0 : patch, commit, tag
git apply v110.patch
git add *
git commit -m "add v1.1.0"
git tag v1.1.0

# v2.0.0 : mk v2-dir, patch, commit, tag
cp foo.go foo.go.bak
git apply v200.patch
# -- make v2 subdirectory --
mkdir v2
mv foo.go v2/
mv foo.go.bak foo.go
cd v2
go mod init "$IMPORT_PATH"/v2
# ------------------------
git add *
git commit -m "add 2.0.0"
git tag v2.0.0
