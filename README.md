# Organising Go Projects

In this tutorial/repo, we will go over some aspects of Go project organisation.
Hopefully, this will answer questions such as:

 -  What are packages and modules
 -  How should project code be organised
 -  What is a workspace

We will introduce concepts in discreet, self-contained steps, numbered for
convenience. Each step will have it's own section in this README, as well as a
directory in this tutorials repo.

For brevities sake, we will assume in the text, that the reader has at least a
cursory look at each sections directory. If things in a section are unclear,
look at the `.go` files and directory structure in the sections directory.

Some sections contain setup/cleanup scripts (written in bash) to make it easier
to reproduce steps described in these sections. While we invite you to have a
look at them, their content will not be described in here much, and reading
them isn't necessary to understand the section they concern.


## Tools of the trade

We will assume you have a recent installation of golang on your machine (at the
time we started writing this, our version was go1.19.3), git, and a text editor
you are familiar with.

This work was written with for with UNIX-like operating systems in mind. While
it should be possible to reproduce most steps in Windows, we cannot guarantee
it. If you want to provide a windows version, that is fine with us, please see
below for contact information.


# License and Contact

This work is released into the public domain under the terms of _The
Unlicense_. See [LICENSE][license] for more info.

If you want to contribute to this work, feel free to send a pull request, or
contact us at mpizka@icloud.com


# 00 - Ancient History

Once upon a time, when Dinosaurs roamed the earth ... okay, not that far back,
but pretty far back anyway. go1.11 was released in August 2018, and very much
changed the way we organise the code of our Go projects.

In this first step, we introduce how things used to work before that, because
we think it's worthwhile to see where some changes came from.

> NOTE: We will use a [publicly available][2] package here from
> `golang.org/x/example/stringutil`. You can of course use your own; just put
> a package in a repo you can reach, and give it a function
> `Reverse(string)string` (you will need to change the import in
> `00/src/myapp/main.go` of course).

First a bit of setup: Run `go env GOPATH` and to check it's current value.
Ideally, it's something along the lines of `~/go`, except written out in full.
Make sure wherever you cloned this repo to is not in this path. If this is the
case;

* `cd src/myapp`

* `go build` and note the error message

* `export GO111MODULE=off`

* `go build`

Okay, interesting, we got 2 different error messages. Let's try something else;

* `cd ../..`

* `export GOPATH=$(pwd)`

* `cd src/myapp`

* `go get`

* `go build`

It worked, and now we have our executable. Moreso, if we go back to the `00`
root directory, and check the contents (especialy of `src`) we may notice a few
things have been downloaded.

> Did you notice that the executable built was named `myapp` instead of `main`?
> Try renaming the directory and build again.

By the way, don't forget to `unset GOPATH` and `unset GO111MODULE` before the
next steps.


## So what's going on here?

Back in the day, installing Go included setting several envvars by the user.
These told Go where to find it's compiler and std packages (GOROOT), and where
to find or download 3rd party packages to. (A 3rd party pkg is everything not
part of the stdlib). The variable that did the latter, was `GOPATH`.

The idea was, that `GOPATH` defined what the documentation called a
"Workspace". The workspace had three sub-directories; `src`, `pkg` and `bin`.

> Note: This isn't the same as what "workspace" means in modern Go.

`src` was where all downloaded packages lived, under a path that mimicked the
import path; eg. if you said `go get example.com/foo/bar`, the go-tool would
download the source code to `$GOPATH/src/example.com/foo/bar`. Packages could
now import this package `bar`;

    import "example.com/foo/bar"

This way, source code was cached locally, and you didn't have to download it on
every build. For convenience, if you ran `go get` in a projects root directory
(as we did above), the go-tool downloaded all external dependencies that were
not already in `$GOPATH/src`

`pkg` was used to store compiled build-artifacts. You can try to download our
dependeny from above directly with `go get golang.org/x/example/stringutil`,
and you will notice a `*.a` file for the package there. If you are already used
to C libraries, this will look familiar.

`bin` was meant for the `go install` command. If your project was an executable
program rather than a library package (meaning, it's package name is `main`),
you could `go install` it, and the executable would be put in `$GOPATH/bin`,
which you could just add to your `$PATH` for convenience. The idea here was a
convenient way to distribute go software to the end user, by downloading,
compiling and installing it to a `$PATH` location in one go (pun intended).


## So what's wrong with that?

This approach had several problems; the most glaring of which: there was no
builtin dependency-management. All packages for all projects on a machine were
stored in `$GOPATH/src`, and since the path to each had to mimick the import
path, there could be exactly one version per package. What if 2 projects needed
different versions of the same package?

People quickly came up with external tools to manage different `GOPATHS` and
vendor-directories for different projects. Okay, but how do we ensure
build-reproducability? What about the promise of Go that all relevant info on
how to do a successful build should be in the source files? Quickly we were
back to 'ye olde days of Makefiles and a dozen different build systems,
different notation mechanisms and whatnot.

Because of this, the `GOPATH` dependent workspace was deprecated in favor of
Modules (more below) in go1.11; in go1.13 the new behavior became the default.
As you have probably guessed, the `GO111MODULE` variable controls whether or
not this new mechanism is active.


# 01 - What's a module

Let's start with a bit of terminology first;


## Import Path

An *import path* tells the go-tool where a package can be found. It is the
string you write after the `import` top level statement. Technically, it's a
path the go-tool can follow; either in the local file-system under
`$GOPATH/src` (pre-module system) or `$GOPATH/pkg/mod/...` (with modules, see
below); or to a VCS system available on the network.

> There are other paths the go-tool may follow with the module system, more on
> that later (vendors, workspaces, ...).


## Package

A *package* (pkg) is a collection of 1 or more `*.go` files that are always
compiled together, they form the smallest unit of compilable code. The
`package` statement denotes which package a source file belongs to. All source
files of a package live in the same directory which *should* be named the same
as the package, and there can only be one package per directory. 

> Actually that last part isn't entirely true, there are ways around this.
> However, they all come with headaches, and sticking to the canonical way is
> preferable in almost every case.

pkg's may be put into sub-directories of other pkg's; these are sometimes
called "sub-packages" A common  example; the stdlib `net` package has an `http`
sub-pkg with the import path `net/http`.

As far as the go-tool is concerned, a sub-pkg doesn't have any special
relationship with its parent pkg; Technially it doesn't matter if `http` is a
sub-pkg of `net`, its own top-level pkg in the stdlib, or a 3rd party pkg
altogether, their relationship is the same.

sub-pkg's exist primarily to logically group packages. It makes sense that
`http`, which relies heavily on `net`, can be found in an import path that
denotes a sub-directory of its primary dependency.


## Module

A *module* (mod) is a collection of packages (1 or more) that are distributed
together, usually because they share a common functionality. Modules provide
the much needed versioning (Packages themselves have no version, but modules
do) and dependency-management. The packages of a module all share a common
root-directory, which contains a `go.mod` file. This file denotes the module
and specifies, among other things, the modules name, which is also its
import-path, minimum required Go-version and it's dependencies (aka. other
modules required to compile this one, with their versions), eg.;

    module golang.org/x/example

    go 1.15

    require golang.org/x/tools v0.0.0-20210112183307-1e6ecd4bf1b0

To see another good example of a module, look no further than your Go
installation. If you `cd $(go env GOROOT)/src` you will find a single `go.mod`
file, telling you that this is the `std` module, and a lot of directories for
the packages and sub-packages of the standard library.


## Repository

Lastly *repositories* should be familiar to everyone who ever used git, because
they are exactly that; git or other version-control-system (VCS) repositories.
A modules import path usually begins with the URL (without schema) of it's
repository, eg. `golang.org/x/example/stringutil`.

In this example, `stringutil` is both the package name and the path of the pkg
directory in the repo.  The repo is named `example` and is hosted at
`golang.org/x`. (If you look up the `src` directory from step 02, you will see
that there is a `.git` dir in `src/golang.org/x/example/`).

The package name is always the last component of the import-path, even if the
pkg is in a sub-sub dir of the repo.

It is perfectly valid to have a repo where a package is served directly from
the repository root. In this case, the repo- and package-name are the same.
This is a common ocurrence for single-mod, single-pkg repositories.

A repository can contain more than one module, although this is usually not
recommended; It makes versioning harder than it has to be (more on that later);
and is impractical because when we `go get IMPORTPATH` the go-tool will always
download the entire repository (because it uses the installed VCS clients in
the background).

This doesn't mean that modules have to live in repositories, or that
import-paths have to point to some location outside of a local project. We will
see later, how this works.

That should be enough terminology for now.


## Working with modules

Let's start working with modules now, by "building" a small module named "foo".
`cd 01/foo` and do the following;

* Run `go test`, which should fail (if not, check if you unset the envars from
  step 00)

* Run `go mod init example.com/foo`

* Look at the newly created `go.mod` file (`cat go.mod`)

The `go mod init` command initializes a new module for us, it's argument is the
modules name (import-path). As you can see, that path doesn't have to point to
an actual directory or VCS path (more on that later). We could have named it
just "foo" as well.

When a directory, (or one of its parents) contains a `go.mod` file, it becomes
a module-directory. Inside a mod-dir, the go-tools work, eg. they know how to
resolve dependencies; Every import path rooted at the module-name has to be in
it's local directory-structure; Every other import path can be resolved to
`$GOPATH/pkg/mod/`, and/or needs to be downloaded. The tools now also know what
the module is called.

We talked about how modules take care of dependency-management, so let's
introduce a dependency next.


# 02 - Dependencies in modules

The upgraded version of pkg `foo` depends on the example pkg mentioned before.
Let's see how this changes things. We already initialized a module in the
directory this time;

* Look at the `go.mod` file, then run `go test`

* Run `go mod tidy`

* Look at `go.mod` again. Notice anything different?

* run `go.test`

* Look at the `go.sum` file (can you guess what this is for?)

The reason why the first test fails, is because `go.mod` didn't yet match the
requirements specified in the source-code. The `go mod tidy` subcommand
resolves this discrepancy, changing `go.mod` along the way. In order to run
tests, the package has to be built (in a temp-directory), in order to do that,
the build-chain needs an overview of all the dependencies.

Wait, doesn't the source code specify the dependency already? Yes and no. It
specifies, by path, a package we want, but remember one of the main purposes of
modules; the source code doesn't specify *which version* of `stringutil` we
want. If you look at `go.mod` after the `tidy` command, you see that it
specifies a VERSION of the package as well as its import path.

> If you think that version looks weird, you are correct. We will explain in
> step 03 why that is. For now, just accept it as it is.

Not only did `go mod tidy` change the mod-file, it also downloaded the
dependency to the cache at `$GOPATH/pkg/mod`.

> NOTE: In older tutorials, you will find other commands like `go test` and `go
> build` update `go.mod` automatically. This behavior was [changed in
> Go1.14][3]. Now only a distinct subset of commands like `go mod tidy` and `go
> get` can update the mod-file.

You may have noticed that there are alot more pkg's listed in the `go.sum`
file, than we import. That's because the one pkg we did import
(`golang.org/x/example/stringutil`) has dependencies as well, which again have
dependencies, etc. This is called "transient dependencies". They are not listed
in our modules `go.mod`, but are present in the mod file of whatever we used.

* Run `cat go.sum`

* Run `cat $(go env GOPATH)/pkg/mod/golang.org/x/example*/go.mod`

* Run `go mod graph`

* Run `go list -m all`

If you want to know which packages your program depends on, you can use the `go
mod graph` and `go list -m all` commands.


# 03 - Dependencies and Versions

In this step, we take a closer look at one of the core functionalities of
modules, which is to provide dependency-management (DM). Wait, we already did
that, no? Yes, but this time we will actually control which version of a
dependency we use (which is one of the major points of DM).

We have our example pkg again, but changed it, so it depends on `v1.0.0` of the
`github.com/google/uuid` module. Look at the source code of `foo.go` and its
tests for a while until you understand what happens.

We already set up a go.mod file, so running `go.test` should work without
problems. If the source is not available in the local module cache
(`$GOPATH/pkg/mod/`), it will be downloaded.

* Run `go test`

* Look at `go.mod`, notice the version required

Let's change the source code: run `patch -b foo.go foo.patch`

> The `patch` program applies a file created with `diff`. The -b flag creates a
> backup of the original. Both programs are part of the unix coreutils.

Now `go.test` fails with the following error; 

    ./foo.go:12:16: undefined: uuid.NewString

That's because we are using an old version of the `uuid` pkg. The function
`NewString()` was added in `v1.2.0`. So, let us upgrade the dependency;

    $ go get github.com/google/uuid
    go: upgraded github.com/google/uuid v1.0.0 => v1.3.0

And now our tests pass again.

> At the time of writing, `v1.3.0` is the latest version of this package. It is
> possible that you will get a newer one.

Wait, how did we get the older version of `uuid` in the first place? We'll come
to that in a moment, but first a word about how versioning works in Go in the
first place.


## Semantic Versioning in Go

Go modules (remember pkg's don't have versions, their module does) use
[semantic versioning][5] (semver). When the go-tool is used to fetch a package,
it can be instructed to fetch a specific version;

* Undo the patch: `mv foo.go.orig foo.go`

* Delete the module files: `rm go.mod go.sum`

* Recreate the module: `go mod init example.com/foo`

* Fetch the `v1.0.0` dependency: `go get github.com/google/uuid@1.0.0`

Your `go.mod` file should now look something like this;

    module example.com/foo

    go 1.18

    require github.com/google/uuid v1.0.0 // indirect

Running the test should work, if you re-apply the patch, the test should fail
again. If you run `go mod graph` you will see that our module depends on
`v1.0.0` of the uuid-pkg;

    $ go mod graph
    example.com/foo github.com/google/uuid@v1.0.0

Cool, but now there are a couple of questions;

1. What's this `// indirect` in `go.mod`?

2. How and where does the go-tool find  specific versions?

3. Why didn't we have to specify a version before, when we upgraded form
   `v1.0.0` before?

4. In step 02, we had long, strange `0.0.0-...` version-numbers, why is that?

Let's answer them in order; `// indirect` means, that the go-tool isn't sure
why we need that dependency in our module yet (there are [other reasons][4], but
this is the most common. Usually this happens if we `go get` something. If we
run `go mod tidy`, the marker is gone, because `tidy` scans the source and
determines what dependencies are actually needed.

The go-tool locates specific versions by looking at the [tags][7] of the
repository the import-path points at. If you look at the tags on the [github
page][6] of google's `uuid` module, you will see that several commits are
tagged using the semantic-version syntax.

When we run `go get` without specifying a version, the go-tool assumes we mean
the `@latest` version. What this means, depends on the available tags in the
repository: The latest tagged stable version is prefered, if none exists; the
latest tagged [prerelease-version][8]; if none exists; the latest untagged
version on the repos default-branch.

> Pre-release versions exists primarily in 2 scenarios; Before a stable version
> has been released, or between stable versions, such as nightly builds,
> release-candidates (rc, [the linux kernel][10] is a good example), etc.

The latter is also the answer to the 4th question above; if you look at the
[repo][9] of the stringutil pkg from before, you will notice that there are no
version-tags (At least not at the time of writing this, my apologies if that
should have changed when you read this). So what the go-tool does, it picks the
latest commit from the default-branch, and writes a pseudo-version into
`go.mod`; at the time I write this it looks like this;

    require golang.org/x/example v0.0.0-20220412213650-2e68773dfca0

; this pseudo-commit has the format `v0.0.0-YYYYmmDDHHMMSS-hash` with the
timestamp being the time of the commit, and hash being the first 6 bytes of the
commit hash (At least that's how this works for git repos, I have no idea if it
would be the same with other VCS's).

So `go get` is your primary tool to specify specific versions for the packages
(really their module) that you use. It can also be used to to up/downgrade or
remove dependencies from your `go mod` file.

> See `go help get | less` for more detailed info

Think of `go get` as a tool not so much to fetch pkg's, but to specify what
versions of packages your project/module should depend on ... in short, to
specify your projects *dependencies*. `go tidy` on the other hand is used to
clean-up the dependencies based on what your source code actually needs.

Bear in mind that `go mod tidy` will not alter existing dependencies, aka. it
will not change the version of a dependency already specified in  `go.mod`. It
will, however, remove dependencies if the source doesn't actually use them, and
add missing dependencies to `go.mod`, using the `@latest` version (see above)
available for that package. If you want to update specific dependencies to the
latest versions, use `go get`.

Because of the above, the `go.mod` file is an integral part of a go project,
and should be checked into your VCS alongside your code.

> You can also get specific commits with `go get PATH@commithash`. This can be
> useful if you want as-yet untagged commits from a project that already
> includes other semantic version tags.


# 04 - Major versions in Go

In this step we will look at major versions of packages, and how the go-tool,
source and repositories deal with them.


## A bit of setup first

To follow along with this step, we need a repo that we can fetch packages from.
Instead of providing one here, or using an existing one, we included a shell
script to build your own;

    cd 04/repo/setup.sh
    setup IMPORT_PATH

Now, we could tell you to push this to a public git repo, but let us offer an
alternative; how about we host our own local gitserver and explore some more Go
envvars and peculiarities along the way?

* Setup a directory somewhere, eg. `/home/me/mygitsrv/foo`

* Copy the `04/repo/*.sh` and `04/repo/*.patch` files to that directory

* Run `setup.sh`

You now have a git repository in your path, with 2 branches (`main`, `v2main`)
and 3 tags (`v1.0.0`, `v1.1.0`, `v2.0.0`). You can run `git log --all --graph`
to have a look.

Now run the following command in `/home/me/mygitsrv` to start git's builtin
server daemon;

    git daemon --verbose --reuseaddr --export-all --base-path=. .

Congratulations, you are now running your very own, readonly, bare-bones, git
server on the default git-port 9418.

In order to be able to use it, you need to export 2 envvars in the directory
from which we will perform the steps in the next section;

    export GONOPROXY=127.0.0.1
    export GONOSUMDB=127.0.0.1

This may look a bit strange, but run with it for now. There will be an
explanation at the end of step 04.


## Updating dependencies between major versions

Have a look at the `main.go` file in this directory. We have already set up a
`go.mod` file naming our project "myapp", but no requirements are specified
yet. Let's change that;

    go mod tidy

The go-tool should now have fetched `v1.1.0` of the foo-dependency, and updated
the go.mod file.

> If you get strange error messages, make sure you set the envvars as described
> above. You may have to clean the module cache after setting them if you ran
> the commands without them set; `go clean -modcache`

If you try building the app now, it should work without a problem. Good! We can
also downgrade the dependency to `v1.0.0`;

    go get 127.0.0.1/foo.git@v1.0.0

So now let's try to upgrade this to the newest version `v2.0.0`. If you
examined the repo, you will have seen that the tag exists, and is on its own
branch (v2main), which is not an unsual workflow for many projects.

> Many projects have a branch for every major release, which makes it easy to
> maintain multiple supported versions at once, and allows for backporting
> changes to older versions.

    $ go get 127.0.0.1/foo.git@v2.0.0
    go: 127.0.0.1/foo.git@v2.0.0: invalid version: go.mod has post-v2 module path \
    "127.0.0.1/foo.git/v2" at revision v2.0.0

Wait, what?

Go doesn't let you jump to another version just by changing the major-number in
the semver. You also need to *change the import path* by appending `/vN` with N
being the major version number. If you inspect the repo (check out the v2main
branch), you will see that the `go.mod` file states a different import path
than in the v1 tagged commits as well;

    module 127.0.0.1/foo.git/v2

So we need to change the import path in `main.go`;

    "127.0.0.1/foo.git/v2"

; now we can get the upgraded version with `go mod tidy`, or fetch it with the
`get` subcommand;

    go get 127.0.0.1/foo.git/v2@v2.0.0

Finally we can build, fix the error in `main()` (because ofc v2 introduced a
breaking API change ;-) ) and finally build again and enjoy our v2 app.


## Explanation time

Right, so ... why does the import path change? There is no `v2` subdirectory in
our repo. And while we're at it: Why can we still access the `Quote()` function
in the `foo` namespace despite our import path ending on `v2`?

Simple answer; Because the go-tool demands that for every semver-major N
greater than 1, the import path has to end on `/vN`. This suffix is not
considered part of the pkg name by the compiler.

There are two reasons for this;

1. We may need to use 2 different major versions of a pkg at the same time
2. We cannot specify pkg versions in the source code, only paths

This is often relevant as project, using a library, when moving to a new major
version of that library tend to *gradually* switch from one version of the API
to another one. Eg. our netcode may still be using the old API because it's
much harder ro rework, while we focus on incorporating the new ORM calls, and
only once we ironed out that, we start shifting the netcode over to the new,
incompatible API.

This requires to specify 2 dependencies on different major versions of the same
package. Because of the second reason, we can't do this;

    import "127.0.0.1/foo.git@v2.0.0"

; so we need a way to specify at least the major version with something the
go-tool accepts as a path. It helps to think of the suffix not so much as a
path component, than a way to specify at least major versions in the source
directly.

> For minor and patch versions, such a mechanism isn't required, because these
> should never introduce a breaking API change.

To try this, let's first undo our changes to `main-go` (you can use `git reset
main.go` for this if you cloned this repo). Then we change it so both versions
of pkg `foo` are used. We add this to the import statements;

    foov2 "127.0.0.1/foo.git/v2"

; and this to `main()`;

    fmt.Println(foov2.Quote(true))

If we now do `go mod tidy` and look at `go.mod` we'll see that both versions of
the package have been added to teh dependencies;

    require (
            127.0.0.1/foo.git v1.1.0
            127.0.0.1/foo.git/v2 v2.0.0
    )

The reason why we renamed the v2 import, is because, as stated above, the
compiler doesn't consider the `/v2` part of the packages name, so without
renaming the second import, we would get an error on building;

    ./main.go:7:5: foo redeclared in this block
            ./main.go:6:5: other declaration of foo

This also helps during the transition from one major version to the next; the
developer immediately sees if `foo()` was called form the new version. Once we
transitioned fully to the newer major version, we can delete the old import and
remove the import alias.


## What about GONOPROXY and GONOSUMDB

We promised an explanation above. The go-tool, by default, doesn't dowload
dependencies directly from VCS's. Instead it contacts a server speaking the
[GOPROXY protocol][11], using a list of servers stored in the go-env `GOPROXY`.
This server performs a couple of lookups on behalf of the go-tool and may also
cache the results.

The go-tool also computes a checksum of each downloaded module and compares
that to it's local `go.sum` file. Failing that (eg. the file doesn't yet exist
or has no checksum for a module, it queries a checksum-database server, using
the list in `GOSUMDB`. This is primarily to ensure that, whether by mistake or
malicious acts, commits haven't been changed after they have been published.

You can query the current value of both these variables;

    go env GOPROXY GOSUMDB

If you want to know more about them, see [here][11] and [here][12]. The
defaults for both these variables are public servers (operated at the time of
writing this by Google), which btw. is how the [pkg search][13] [works][14].

Of course, these public servers cannot access your locally hosted repo at
`127.0.0.1`, which is why we use the `GONOPROXY/SUMDB` variables. These are
lists of packages, services, etc. that you don't want to have handled via the
proxy, or for which you don't want the go-tool to query the GOSUMDB.


# 05 - What if it's a path after all?

You may have wondered why the syntax to declare a semver-major looks so much
like a path. One answer is that the compiler only accepts paths as imports.
Another is, that they can actually be paths ... in fact, that's the officially
recommended method.

This step will demonstrate this (and hopefully not be as long as 04). Prepare
your local `foo` repo the same way as described above (don't forget to set the
`GONO...` vars and run `go clean -modcache`.

Check the new `foo` repo. There is only one branch (`main`) and the code for v2
lives in a subdirectory `/v2`, with the code from the v1 still intact. You can
now do the same exercises as in 04, and never know the difference.

The idea here is, that instead of developing our different major versions on
different branches, we develop them in different paths, maintaining a copy of
the project in `/vN` subdirectories.

> Don't mix this up with sub-modules, even though it also puts a `go.mod` file
> into a subdirectory of a module.

This approach has pros and cons, the most obvious being that it plays nicely
with [trunk-based-development][15]. This git-workflow doesn't allow for long
running branches other than the trunk, so you have to check everything into the
mainline.

The downside is, that you duplicate the code, and it becomes less obvious to
people not familiar with this stile to figure out what is going on when looking
at the repo. Also, when tagging new versions for your v1 (which you may still
be maintaining ater v2 was released, your tag-history may look strange, with
v1-tags being intermixed with v2-tags.

Both these workflows work, the go-tool doesn't care which one you use, so find
what works best for you, your team, your project.


# 06 - More fun with go get

If you looked at the repos and the code in the steps 04 & 05, you may have
noticed that our modules import path ended in `.git` both in the code and in
the imported modules `go.mod`. The question why, is perfect for a little
segue into the inner workings of `go get`.

This step requires a little setup as well;

* Empty the root dir of the local git server established in step 04 (we assume
  here that's `home/me/mygitsrv`);

* Copy the `06/repo` dir into it twice;

    cp -vr 06/repo /home/me/mygitsrv/foo
    cp -vr 06/repo /home/me/mygitsrv/bar

* In both these directories run `setup.sh` with the following arguments, this
  will setup two repos with go modules, names after the 2nd script argument;

    # in bar/
    setup.sh 127.0.0.1 bar
    # in foo/
    setup.sh 127.0.0.1 foo

* Build the little webserver in `06/server`, then copy its binary to repo-root

    cd 06/server
    go build
    cp server /home/me/mygitsrv

* In the session where you will run the go-tool commands, make sure
  `GONOPROXY/SUMDB` are set as described earlier, and `GOINSECURE`;

    export GOINSECURE=127.0.0.1

Now run your `git daemon` server in the repo-root, and start the http-server
(we recommend running them in 2 different terminal windows so you can see both
working). The server has to be started with root permissions, as it will bind a
low socket (80 HTTP);

    git daemon --verbose --reuseaddr --export-all --base-path=. .
    sudo server

We now have 2 repositories, each holding a go-module (`foo`, `bar`) and a small
server application that can answer HTTP GET requests.

> You don't have to use 127.0.0.1 as the host btw.; if you have a name-service
> like `avahi-daemon` you can use that as well, just `setup.sh` the repos with
> the correct hostname (first argument), and run `server -host HOSTNAME`.
> Remember to add this HOSTNAME to the exported `GONOPROXY/SUMDB` and
> `GOINSECURE`. For the remainder of this session, we will assume everything is
> running using `127.0.0.1` as host.

Now have a look at `06/main.go`. The source imports package `127.0.0.1/foo` and
`127.0.0.1/bar/v2`. The import path no longer ends with `.git` , and if you
look in the created repos, the `go.mod` files no longer have `.git` in their
path either.

If you now run `go mod tidy` in the 06 directory, you should see multiple
queries to the web-, as well as to the git-server, and the download should be
successfull (you may need to run `go clean -modcache` again).


## Explanation time again

So what exactly was the purpose of that `.git` suffix, and what does the
webserver have to do with all that?

This is explained pretty well in `go help importpath | less`, so we will just
give a short version here; Remember that we said the go-tools use existing VCS
client programs like `git` in the background to actually download the repos?
Well, the go-tool supports more than just `git`; Bazaar, Fossil, Mercurial and
Subversion are all on the menu as well, begging an important question: How does
the go-tool know which one to use for a given import-path?

Except for a few known hosts like github.com, go get relies on the import-path
to specify which VCS it should use to talk to the repo. That is what the `.git`
suffix in our import paths previously did; telling the go-tool "Use git to
fetch this".

If the import-path doesn't specify a VCS, and the url isn't one of a few
exceptions, the go-tool has to somehow determine what VCS to use. It does that
by sending an HTTPS GET request to the import path (or HTTP, if the path or its
root appear in the `GOINSECURE` listing envvar);

    http://127.0.0.1/bar/?go-get=1

In the response, it expects to find an HTML
document containing a special `<meta>` tag explaining what VCS to use and how
to contact it, eg.;

    <meta name="go-import" content="127.0.0.1/bar git git://127.0.0.1/bar">

> The tag doesn't even require a HTML document around it, just returning the
> tag is sufficient

The go-tool basically asks "Hi, go-get here, I'm looking for pkg
`127.0.0.1/bar`, can you help me?" and the server replies "Sure, package
127.0.0.1/bar is in a git repo reachable at `git://127.0.0.1/bar`".

By adhering to this protocol, VCS hosting services, including private
repositories, can guide the go-tool to repository servers and without requiring
the import paths to hold that information. It's also possible to alias repos
that way, eg. this response (tag left out for brevity);

    example.com/foo git https://vcs.example.com/git/foo

; would allow importing `example.com/foo` whereas the actual repo is hosted on
the companies dedicated VCS system.


# 07 - Vendoring

We looked at goproxies already earlier, and how the go-tool uses them to
download pkg's indirectly. We didn't mention alot of reasons why they are used
in the first place, other than there being a public package index. The most
important reason for GOPROXY to exist, is to prevent builds from breaking
because a package is no longer reachable ([the left-pad incident][16] anyone?).

If a build imports `import "example.com/foo"`, and example.com goes down, or
whoever wrote the pkg decides to delete the repo, the build would fail. To
prevent such a situation, public goproxies *cache* pkg's they download on the
go-tools behalf, and so there is a good chance that builds will continue to
succeed even if there is a problem with their dependencies source.

But what if I want to be really, really, REALLY sure that a build can be made
no matter what happens to the original source or any proxies (At least while my
own repo is still intact)?

Such concerns are one of the reasons why *Vendoring* exists; making a local
copy of the source code of all the dependencies that a project uses, so even if
no remote resource is reachable, a build can still be done from scratch.

We could of course download the sources manually and build custom import paths,
but this is completely impractical to do for even mid-sized projects. Luckily,
the go-tool got us covered. We will use a little LeastRecentlyUsed
implementation `lru.go` and it's tests as a demonstration. `lru` depends on
`github.com/google/uuid` to generate the uuid keys of the LRU-cache. Run the
tests to see that some of these keys in the output

    go test

Next we download all dependencies;

    go mod vendor

You will now see a `vendor` directory in the project dir, containing the
dependencies code in a directory structure similar to the import path, as well
as a manifest `modules.txt`.

This directory isn't just a backup copy. Since go1.14, if this directory is
present, `go build` will use the source from that directory for its builds (`go
help build | less` and search for "vendor")

This has some interesting consequences; one, if we *change* the dependencies
code in the `vendor` dir, our application will be built with that change. You
can try this for example by changing the code in the uuid modules file
`uuid.go` in the function String() (line 185), so that the returned uuid
Strings are uppercased.

Two, if we check this vendored code in together with our own, we now have a
repo from which we can do builds without relying on external resources;
everythign needed to build is on our own harddrive.

> NOTE: When doing that, always check for the LICENSE of the code thet is being
> vendored, to make sure this is allowed.



<!--
vendor -> overwrite changes, fragility, over to next step: redirect
          also see docs
workspaces
More detail about GOPROXY and GOSUMDB
-->



<!--LINKS-->

[1]: https://go.dev/blog/using-go-modules
[2]: https://pkg.go.dev/golang.org/x/example
[3]: https://go.dev/doc/go1.14#go-command
[4]: https://github.com/golang/go/wiki/Modules#why-does-go-mod-tidy-record-indirect-and-test-dependencies-in-my-gomod
[5]: https://semver.org/
[6]: https://github.com/google/uuid/tags
[7]: https://git-scm.com/book/en/v2/Git-Basics-Tagging
[8]: https://semver.org/#spec-item-9
[9]: https://go.googlesource.com/example
[10]: https://github.com/torvalds/linux/tags
[11]: https://go.dev/ref/mod#module-proxy
[12]: https://golang.org/ref/mod#authenticating
[13]: https://pkg.go.dev/
[14]: https://pkg.go.dev/about#adding-a-package
[15]: https://trunkbaseddevelopment.com/
[16]: https://www.infoworld.com/article/3047177/how-one-yanked-javascript-package-wreaked-havoc.html
