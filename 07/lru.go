// package lru implememts a simple LRU cache
package lru

import (
    "fmt"

    "github.com/google/uuid"
)

// KeyNotFoundError implements errors.Error
type KeyNotFoundError struct {
    Key string
}

func (e KeyNotFoundError) Error() string {
    return fmt.Sprintf("Key not found: '%s'", e.Key)
}


// node represents a single storage node in the double-linked-list this
// implementation uses to store the actual data
type node struct {
    key string
    data any
    next *node
    prev *node
}


// Cache is a simple LRU cache. The zero value of Cache can be used immediately
// with a storage limit of 100 items.
type Cache struct {
    index map[string]*node  // key->node map for fats lookup wo. dll traversal

    mru *node   // first...
    lru *node  // ...last node of backing storage

    limit int   // maximum size
    size int    // current size in items
}


// Create a new cache with limit l
func NewCache(l int) (c *Cache) {
    if l <= 0 {
        panic("bad limit")
    }
    c = &Cache{limit: l}
    c.init()
    return c
}


// initialize the index map if Cache is at zero value
func (c *Cache) init() {
    if c.index != nil {
        return
    }
    if c.limit == 0 {
        c.limit = 100
    }
    c.index = make(map[string]*node, c.limit)
}


// Put an item into the cache. For zero-Caches, this will initialize the Cache to it's default size (100)
func (c *Cache) Put(item any) (key string) {

    c.init()

    // if full: evict LRU item
    if c.size >= c.limit {
        delete(c.index, c.lru.key)
        c.size--
        if c.size == 0 {
            c.mru = nil
            c.lru = nil
        } else {
            c.lru.prev.next = nil
            c.lru = c.lru.prev
        }
    }

    // add new element
    key = uuid.NewString()
    n := node{key: key, data: item}
    c.index[key] = &n
    if c.size == 0 {
        c.mru = &n
        c.lru = &n
    } else {
        c.mru.prev = &n
        n.next = c.mru
        c.mru = &n
    }
    c.size++

    return key
}


// Get an item from the cache and return its value. Returns KeyNotFoundError if the key doesn't exist.
func (c *Cache) Get(key string) (value any, err error) {

    c.init()

    if c.size == 0 {
        return nil, KeyNotFoundError{key}
    }

    n, ok := c.index[key]
    if !ok {
        return nil, KeyNotFoundError{key}
    }
    
    // move the node to MRU position
    // don't need to do anything if size=1 or x node=MRU already
    if c.mru != n && c.size > 1 {

        // remove the node:w
        if n.next != nil {
            n.next.prev = n.prev
        }
        if n.prev != nil {
            n.prev.next = n.next
        }
        if c.lru == n {
            c.lru = n.prev
        }

        // put into MRU position
        c.mru.prev = n
        n.next = c.mru
        n.prev = nil
        c.mru = n
    }

    return n.data, nil
}

