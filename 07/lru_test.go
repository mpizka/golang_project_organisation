package lru

import "testing" 

func Test1(t *testing.T) {
    c := NewCache(5)
    keys := []string{}

    for i:=0; i<5; i++ {
        k := c.Put(i)
        keys = append(keys, k)
        t.Logf("put %v : %v", i, k)
    }

    for i, k := range keys {
        v, err := c.Get(k)
        if err != nil {
            t.Error(err)
        }
        if v.(int) != i {
            t.Errorf("key %v: expected=%v got=%v", k, i, v)
        }
    }
}


func Test2(t *testing.T) {
    c := NewCache(5)

    for i:=0; i<5; i++ {
        k := c.Put(i)
        t.Logf("put %v : %v", i, k)
    }


    keys := []string{}
    for i:=0; i<5; i++ {
        k := c.Put(i)
        keys = append(keys, k)
        t.Logf("put %v : %v", i, k)
    }

    if c.size != 5 {
        t.Errorf("expected size 5, got %v", c.size)
    }

    for i, k := range keys {
        v, err := c.Get(k)
        if err != nil {
            t.Error(err)
        }
        if v.(int) != i {
            t.Errorf("key %v: expected=%v got=%v", k, i, v)
        }
    }
}

func Test3(t *testing.T) {
    c := NewCache(3)

    c.Put(1)
    k2 := c.Put(2)
    c.Put(3)
    c.Put(4)

    a := [3]int{}
    test := [3]int{4,3,2}
    for n, i := c.mru, 0; n != nil; n, i = n.next, i+1 {
        t.Logf("%v", n.data)
        a[i] = n.data.(int)
    }
    if a != test {
        t.Errorf("expected=%v got=%v", test, a)
    }

    c.Get(k2)

    a = [3]int{}
    test = [3]int{2,4,3}
    for n, i := c.mru, 0; n != nil; n, i = n.next, i+1 {
        t.Logf("%v", n.data)
        a[i] = n.data.(int)
    }
    if a != test {
        t.Errorf("expected=%v got=%v", test, a)
    }
}

func Test4(t *testing.T) {
    c := NewCache(1)

    for i:=0; i<1000; i++ {
        c.Put(1)
    }

    k := c.Put(-42)
    v, err := c.Get(k)
    if err != nil {
        t.Error(err)
    }
    if v.(int) != -42 {
        t.Errorf("key: expected=-42 got=%v", v.(int))
    }
    if c.mru.data.(int) != -42 {
        t.Errorf("mru: expected=-42 got=%v", v.(int))
    }
    if c.lru.data.(int) != -42 {
        t.Errorf("lru: expected=-42 got=%v", v.(int))
    }
}

