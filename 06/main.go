package main

import (
    "fmt"

    "127.0.0.1/foo"
    "127.0.0.1/bar/v2"
)

func main() {
    fmt.Println(foo.Quote())
    fmt.Println(bar.Quote(true))
}
