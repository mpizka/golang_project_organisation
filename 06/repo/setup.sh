#! /bin/bash
HOST="127.0.0.1"
REPONAME="foo"

if [[ -n "$1" ]]; then
    HOST="$1"
fi

if [[ -n "$2" ]]; then
    REPONAME="$2"
fi

IMPORT_PATH=$HOST/$REPONAME

# init repository
git init .

# init go project
go mod init "$IMPORT_PATH"

# init ___.go
cat > $REPONAME.go <<EOF
// Package $REPONAME provides some functionality
package $REPONAME


import (
    "math/rand"
)

var quotes = []string {
    "99 little bugs in the code. 99 little bugs in the code. Take one down, patch it around. 127 little bugs in the code...",
    "Always code as if the person who ends up maintaining your code will be a violent psychopath who knows where you live.",
    "How many programmers does it take to change a light bulb? None, that’s a hardware problem.",
    "When I wrote this code, only God and I understood what I did. Now only God knows.",
    "One man’s crappy software is another man’s full-time job.",
    "Documentation is like sex: When it is bad, it is better than nothing.",
    "What’s the object-oriented way to get wealthy? Inheritance.",
    "There are 2 hard things in Programming: naming things, cache invalidation, and off-by-one errors",
    "There are 10 types of people in the world: Those who understand binary, and those who don't.",
    "There’s no place like 127.0.0.1.",
}

func Quote() string {
    return quotes[rand.Intn(len(quotes))]
}
EOF

# init .gitignore
cat > .gitignore <<EOF
*.sh
*.swp
*.patch
*.idea
*.note
a.out
EOF

# v1.0.0 : commit and tag 
git add .gitignore *
git commit -m "add v1.0.0"
git tag v1.0.0

# v1.1.0 : patch, commit, tag
mv $REPONAME.go foo.go
git apply v110.patch
mv foo.go $REPONAME.go
git add *
git commit -m "add v1.1.0"
git tag v1.1.0

# v2.0.0 : branch, patch, commit, tag
git branch "v2main"
git checkout "v2main"
# -- change module path --
rm go.mod
go mod init "$IMPORT_PATH"/v2
# ------------------------
# patch works only with foo.go
mv $REPONAME.go foo.go
git apply v200.patch
mv foo.go $REPONAME.go
git add *
git commit -m "add 2.0.0"
git tag v2.0.0

