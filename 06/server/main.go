// A simple webserver to answer `go get` requests to locally `git daemon`
// hosted repos The server will listen for the incoming HTTP GET request from
// `go get`, replying with a meta-tag containing the VCS system (git) specifier
// and the git:// URI where the go-tool can contact the git-server.
//
// A server running the git-protocol must be present. Since this server runs
// HTTP only, GOINSECURE should be set for the repo host.
package main

import (
    "net/http"
    "log"
    "os"
    "flag"
)

func main() {

    // Define flags
    var host, dir string
    flag.StringVar(&host, "host", "127.0.0.1", "repo hostname")
    flag.StringVar(&dir, "dir", ".", "gitserver rootdir")
    flag.Parse()

    // Scan -dir, add endpoint for every subdir
    entries, err := os.ReadDir(dir)
    if err != nil {
        log.Fatal(err)
    }
    for _, e := range entries {
        if e.IsDir() {
            name := e.Name()
            url := "/" + name + "/"
            meta := `<meta name="go-import" content="` + host + `/`+ name + ` git git://` + host + `/` + name +`">`
            http.HandleFunc(url, func (w http.ResponseWriter, r *http.Request) {
                log.Printf("[%s] %s", r.RemoteAddr, r.RequestURI)
                doc := []byte(meta)
                w.Write(doc)
            })
            log.Printf("added handler for %s => %s", url, meta)
        }
    }

    // Add catchall root-endpoint (for logging)
    http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
        log.Printf("[ROOTPATH] [%s] %s", r.RemoteAddr, r.RequestURI)
        http.Error(w, "the pkg you seek is not here", 404)
    })

    // Start server
    log.Printf("listening on %s", host)
    log.Fatal(http.ListenAndServe(":80", nil))

}
