package foo

import (
    "golang.org/x/example/stringutil"
)

func Foo() string {
    return "foo, bar, baz are common placeholder-names"
}

func Oof() string {
    return stringutil.Reverse(Foo())
}
